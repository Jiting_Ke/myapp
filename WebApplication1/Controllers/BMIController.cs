﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModel;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index() {
            return View(new BMIData());
        }
        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            /*if (data.height < 50 || data.height > 200) {
                ViewBag.HeightError = "身高請輸入50-200的數值";
            }
            if (data.weight < 30 || data.weight > 150)
            {
                ViewBag.WeightError = "體重請輸入30-150的數值";
            }*/
            if (ModelState.IsValid) {
                //ViewBag.H = height;
                //ViewBag.W = weight;

                var m_height = data.height.Value / 100;
                var result = data.weight / (m_height * m_height);

                data.Result = result;
                var level = "";

                if (result < 18.5) {
                    level = "體重過輕";
                }
                else if (18.5 <= result && result < 24 )
                {
                    level = "正常範圍";
                }
                else if (24 <= result && result < 27)
                {
                    level = "過重";
                }
                else if (27 <= result && result < 30)
                {
                    level = "輕度肥胖";
                }
                else if (30 <= result && result < 35)
                {
                    level = "中度肥胖";
                }
                else if (27 <= result && result < 30)
                {
                    level = "重度肥胖";
                }
                data.level = level;
            }
            return View(data);
        }
    }
}