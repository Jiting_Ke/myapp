﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["aaa"] = "111";
            ViewBag.Name = "Candy";

            ViewData["A"] = 1;
            ViewData["B"] = 2;

            ViewBag.a = 1;
            ViewBag.b = 2;
            return View();
        }

        public ActionResult Html()
        {
            return View();
        }

        public ActionResult HtmlHelper()
        {
            return View();
        }

        public ActionResult Razor()
        {
            return View();
        }
    }
}